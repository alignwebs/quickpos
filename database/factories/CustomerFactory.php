<?php

use Faker\Generator as Faker;

$factory->define(App\Customer::class, function (Faker $faker) {
    return [
       'name' => $faker->name,
       'mobile' => $faker->e164PhoneNumber,
       'address' => $faker->address,
    ];
});

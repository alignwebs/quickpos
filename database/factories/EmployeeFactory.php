<?php

use Faker\Generator as Faker;

$factory->define(App\Employee::class, function (Faker $faker) {
    return [
       'name' => $faker->name,
       'mobile' => $faker->e164PhoneNumber,
       'address' => $faker->address,
       'emp_type' => 'delivery'
    ];
});

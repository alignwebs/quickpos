<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('type');
          $table->string('remark')->nullable();
          $table->date('date');
          $table->date('from_date')->nullable();
          $table->date('to_date')->nullable();
          $table->integer('employee_id');
          $table->string('paid_to')->nullable();
          $table->string('payment_mode')->nullable();
          $table->timestamps();
          $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}

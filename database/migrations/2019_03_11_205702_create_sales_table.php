<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('delivery_employee_id');
            $table->double('delivery_fee',8,2);
            $table->double('amount_payable',8,2);
            $table->double('amount_paid',8,2)->nullable();
            $table->string('comments')->nullable();
            $table->string('status');
            $table->date('date');
            $table->timestamps();
            $table->softDeletes();

            $table->engine = 'InnoDB';
        });

        \DB::statement("ALTER TABLE sales AUTO_INCREMENT = 1000;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}

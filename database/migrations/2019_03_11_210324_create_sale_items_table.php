<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sale_id')->unsigned();
            $table->integer('item_id');
            $table->integer('item_price_id');
            $table->string('item_name');
            $table->integer('dealer_id');
            $table->string('dealer_name');
            $table->float('quantity',8,2);
            $table->double('cost_price',8,2);
            $table->double('sell_price',8,2);
            $table->boolean('dealer_paid')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('sale_id')->references('id')->on('sales')->onDelete('cascade');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_items');
    }
}

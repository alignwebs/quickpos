<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use SoftDeletes;
	
    public function price()
   	{
   		return $this->hasMany(ItemPrice::class);
   	}
}

<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ItemPrice extends Model
{
    use SoftDeletes;

	protected $fillable = ['item_id', 'dealer_id','cost_price','sell_price']; 

    public function item()
   	{
   		return $this->belongsTo(Item::class);
   	}

   	public function dealer()
   	{
   		return $this->belongsTo(Dealer::class);
   	}
}

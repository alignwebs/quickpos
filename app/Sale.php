<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use SoftDeletes;

    public function customer()
       {
           return $this->belongsTo('App\Customer');
       }
       
    public function employee()
       {
           return $this->belongsTo('App\Employee','delivery_employee_id');
       }

    public function sale_item()
   	{
   		return $this->hasMany(SaleItem::class);
   	}
}

<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

	use SoftDeletes;
	
	protected $fillable = ['name','mobile','address'];

    public function sales()
    {
    	return $this->hasMany(Sales::class);
    }
}

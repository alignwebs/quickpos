<?php 
if (! function_exists('ModelBtn')) {

    function ModelBtn($route,$id,$title="")
    {
        $html = '<div class="btn-group btn-options" role="group">';

        $html.= Form::open(['route' => [$route.'.destroy', $id], 'method' => 'delete', 'class' => 'deleteFrm']);
        $html.= '<a href="javascript:void(0)" onclick="edit('.$id.')" class="btn btn-default btn-sm"><span class="fa fa-edit" aria-hidden="true"></span></a>&nbsp;&nbsp;';
        $html.= '<button type="submit" class="btn-delete btn btn-default btn-sm delete-btn" data-title="'.$title.'"><span class="fa fa-trash" aria-hidden="true"></span></button>';
        $html.= Form::close();

        $html.= '</div>';
        return $html;
    }
}

function getEmpTypes()
{
	return [ 
        'delivery' => 'Delivery',
        'manager' => 'Manager'

    ];
}

if (! function_exists('status')) {
function status()
{
    return [ 
        'processing' => 'Processing',
        'out for delivery' => 'Out For Delivery',
        'delivered' => 'Delivered',
        'cancelled' => 'Cancelled'

    ];
}

}

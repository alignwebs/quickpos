<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use Validator;

class EmployeeController extends Controller
{
/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
       
        return view('employees.index');
    }

    public function list(Request $request)
    {
        $rows = Employee::all();
        return view('employees.ajax.list', compact('rows'));
    }

    public function listAjax(Request $request)
    {
        return Employee::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                
            'name' => 'required',
            'mobile' => 'required|digits:10',
            'address' => 'required',
            'emp_type' => 'required',

               
        ]);


        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

      

        $Employee = new Employee;

        $Employee->name = $request->input('name');
        $Employee->mobile = $request->input('mobile');
        $Employee->address = $request->input('address');
        $Employee->emp_type = $request->input('emp_type');

        $Employee->save();


        return response()->json(['success'=>'true','message'=>'Employee has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Employee $employee)
    {
        $id = $request->id;

        $employee = Employee::where('id',$id)->first();
        
        return view('employees.ajax.edit',compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function updateEmployee(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
                
            'name' => 'required',
            'mobile' => 'required|digits:10',
            'address' => 'required',
            'emp_type' => 'required',

               
        ]);


        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

      

        $Employee = Employee::find($request->id);

        $Employee->name = $request->input('name');
        $Employee->mobile = $request->input('mobile');
        $Employee->address = $request->input('address');
        $Employee->emp_type = $request->input('emp_type');

        $Employee->save();


        return response()->json(['success'=>'true','message'=>'Employee has been updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        flash('Employee Deleted Successfully')->success();
        return redirect()->back();
    }
}

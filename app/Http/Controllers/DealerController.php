<?php

namespace App\Http\Controllers;

use App\Dealer;
use Illuminate\Http\Request;
use Validator;


class DealerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
       
        return view('dealers.index');
    }



    public function list()
    {
        $rows = Dealer::all();

        return view('dealers.ajax.list', compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        
        $validator = Validator::make($request->all(), [
                
                'name' => 'required',
                'address' => 'required'
        ]);


        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

      

        $Dealer = new Dealer;

        $Dealer->name = $request->input('name');
        $Dealer->loc_lat = $request->input('loc_lat');
        $Dealer->loc_lon = $request->input('loc_lon');
        $Dealer->delivery_range_km = $request->input('delivery_range_km');
        $Dealer->address = $request->input('address');
        $Dealer->city = $request->input('city');
        $Dealer->state = $request->input('state');
        $Dealer->country = $request->input('country');

        $Dealer->save();


        return response()->json(['success'=>'true','message'=>'Dealer has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function show(Dealer $dealer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Dealer $dealer)
    {
        $id = $request->id;

        $dealer = Dealer::where('id',$id)->first();
        
        return view('dealers.ajax.edit',compact('dealer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function updateDealer(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
                
                'name' => 'required',
               
        ]);


        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

        
        $Dealer = Dealer::find($request->id);
        


        $Dealer->name = $request->input('name');
        $Dealer->loc_lat = $request->input('loc_lat');
        $Dealer->loc_lon = $request->input('loc_lon');
        $Dealer->delivery_range_km = $request->input('delivery_range_km');
        $Dealer->address = $request->input('address');
        $Dealer->city = $request->input('city');
        $Dealer->state = $request->input('state');
        $Dealer->country = $request->input('country');

        $Dealer->save();


        return response()->json(['success'=>'true','message'=>'Dealer has been updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dealer $dealer)
    {
        $dealer->delete();
        flash('Dealer Deleted Successfully')->success();
        return redirect()->back();
    }
}

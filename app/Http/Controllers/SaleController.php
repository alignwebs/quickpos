<?php

namespace App\Http\Controllers;

use App\Sale;
use App\Customer;
use App\SaleItem;
use Illuminate\Http\Request;
use Validator;
use DB;
class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
       
        return view('sales.index');
    }



    public function list()
    {
        $rows = Sale::all();
        
        return view('sales.ajax.list', compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Display the specified resource.
     *
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function show(Sale $sale)
    {
        return view('sales.show',compact('sale'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Sale $sale)
    {
        $id = $request->id;

        $sale = Sale::where('id',$id)->first();
        
        return view('sales.ajax.edit',compact('sale'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function updateEmployee(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
                
            'name' => 'required',
            'mobile' => 'required|digits:10',
            'address' => 'required',
            'emp_type' => 'required',

               
        ]);


        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

      

        $Sale = Sale::find($request->id);

        $Sale->name = $request->input('name');
        $Sale->mobile = $request->input('mobile');
        $Sale->address = $request->input('address');
        $Sale->emp_type = $request->input('emp_type');

        $Sale->save();


        return response()->json(['success'=>'true','message'=>'Sale has been updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        $sale->delete();
        SaleItem::where('sale_id',$sale->id)->delete();
        flash('Sale Deleted Successfully')->success();
        return redirect()->back();
    }

    public function add(Request $request)
    {   
           $input = $request->all();
           $validator = Validator::make($input['customer'], [
                   
               'name' => 'required',
               'mobile' => 'required|digits:10',
               'address' => 'required',
                  
           ]);

           if ($validator->fails())
               return response()->json(['error'=>true,'info'=>$validator->errors()->first()]);
           
           $Customer = Customer::firstOrCreate(
                        ['mobile' => $input['customer']['mobile']],
                        ['name' => $input['customer']['name'], 'mobile' => $input['customer']['mobile'],'address' => $input['customer']['address']]
                    );

           DB::beginTransaction();

           try {
                $Sale = new Sale;
                $Sale->customer_id = $Customer->id;
                $Sale->delivery_employee_id = $input['delivered_employee_id'];
                $Sale->delivery_fee = $input['deliveryFee'];
                $Sale->km_per = $input['km']['per'];
                $Sale->km_distance = $input['km']['distance'];
                $Sale->km_amount = $input['km']['amount'];
                $Sale->amount_payable = $input['grandTotal'];
                $Sale->comments = isset($input['comments']) ? $input['comments'] : NULL;
                $Sale->status = 'delivered';
                $Sale->date = (!empty($input['date']) ? $input['date'] : date('Y-m-d'));

                $Sale->save();

                foreach($input['cartItems'] as $key => $cart) 
                    $input['cartItems'][$key]['sale_id'] = $Sale->id;

                SaleItem::insert($input['cartItems']);

                DB::commit();

                $sale_sms = "MeatWayy Order #$Sale->id\n[Customer Details]\n$Customer->name\n$Customer->mobile\n$Customer->address\n\n[Order Details]\n";

                foreach($input['cartItems'] as $key => $cart) 
                    $sale_sms.= $cart['item_name']."-".$cart['quantity']."KG\n";

                $sale_sms.= "\nAmount = Rs. $Sale->amount_payable \n\n";

                $customer_sale_sms = $sale_sms;

                $sale_sms.= "[Comments]\n$Sale->comments";

                @file_get_contents('http://api.msg91.com/api/sendhttp.php?route=4&sender=MEATWY&mobiles='.$Customer->name.'&authkey=242206AZQMXiYat8j5bbee55d&message='.urlencode($customer_sale_sms).'&country=91');

                return response()->json(['success'=>true,'sale_id'=>$Sale->id, 'sale_sms'=>$sale_sms]);
              
           } catch (\Exception $e) {
               DB::rollback();
               return response()->json(['error'=>true,'message'=>$e->getMessage()]);
           }
       
    }

    public function amountPaidUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
                
            'amount_paid' => 'required'
               
        ]);


        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

        

        $Sale = Sale::find($request->id);

        $Sale->amount_paid = $request->input('amount_paid');


        $Sale->save();


        return response()->json(['success'=>'true','message'=>'Amount has been updated successfully']);
    }


    public function statusUpdate(Request $request)
    {
       

        $Sale = Sale::find($request->id);

        $Sale->status = $request->input('status');


        $Sale->save();


        return response()->json(['success'=>'true','message'=>'Status has been updated successfully']);
    }
}

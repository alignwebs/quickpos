<?php

namespace App\Http\Controllers;

use App\Item;
use App\Dealer;
use App\ItemPrice;
use Illuminate\Http\Request;
use Validator;
use DB;
class ItemController extends Controller
{
 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
        $dealers = Dealer::all();
        
        return view('items.index', compact('dealers'));
    }



    public function list()
    {
        $rows = Item::all();

        return view('items.ajax.list', compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       
        $validator = Validator::make($request->all(), [
                
                'name' => 'required|unique:items,name',
                'image' => 'nullable|mimes:jpeg,bmp,png',
                'cost_price.*' => 'required',
                'sell_price.*' => 'required',
               
        ]);


        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }


        $path = null;
        if ($request->hasFile('image')) {

           $getClientOriginalExtension = $request->file('image')->getClientOriginalExtension();

           $path = $request->file('image')->storeAs('images',time().".".$getClientOriginalExtension);

        }

        $Item = new Item;

        $Item->name = $request->input('name');
        $Item->image = $path;


        DB::beginTransaction();

        try {
            
            $Item->save();

            $count = sizeof($request->dealer_id);

            for ($i=0; $i < $count; $i++) {
                
            ItemPrice::updateOrCreate(

                    ['item_id' => $Item->id, 'dealer_id' => $request->dealer_id[$i]],
                    ['cost_price' => $request->cost_price[$i], 'sell_price' => $request->sell_price[$i]]
                );

            }

            DB::commit();
           
        } catch (\Exception $e) {
            
            DB::rollback();
            
            return response()->json(['errors'=>$e->errorInfo]);

        }


        return response()->json(['success'=>'true','message'=>'Item has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Item $item)
    {
        $id = $request->id;
        $dealers = Dealer::all();
        $itemPrices = ItemPrice::where('item_id',$id)->get();
        $item = Item::where('id',$id)->first();
        
        return view('items.ajax.edit',compact('item','dealers','itemPrices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function updateItem(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
                
                
                'name' => 'required|unique:items,name,'.$request->id,
                'cost_price.*' => 'required',
                'sell_price.*' => 'required',
               
        ]);


        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

        
        $Item = Item::find($request->id);
        


        $Item->name = $request->input('name');

        if ($request->hasFile('image')) {

           $getClientOriginalExtension = $request->file('image')->getClientOriginalExtension();


           $path = $request->file('image')->storeAs('images',time().".".$getClientOriginalExtension);

           $Item->image = $path;

        }

        $Item->save();

        $count = sizeof($request->dealer_id);

        for ($i=0; $i < $count; $i++) {
            
        ItemPrice::updateOrCreate(

                ['item_id' => $request->id, 'dealer_id' => $request->dealer_id[$i]],
                ['cost_price' => $request->cost_price[$i], 'sell_price' => $request->sell_price[$i]]
            );

        }

        return response()->json(['success'=>'true','message'=>'Item has been updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $item->delete();
        flash('Item Deleted Successfully')->success();
        return redirect()->back();
    }

    public function itemList()
    {
        $items = Item::with('price.dealer')->get();
        return $items;
        return response()->json(['count'=>$items->count(),'items'=>$items]);
    }
}

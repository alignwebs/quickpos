<?php

namespace App\Http\Controllers;

use App\Expense;
use App\Employee;
use Illuminate\Http\Request;
use Validator;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $employees = Employee::pluck('name','id');
        return view('expenses.index', compact('employees'));
    }



    public function list()
    {
        $rows = Expense::all();

        return view('expenses.ajax.list', compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(), [
                
                'type' => 'required',
                'date' => 'required',

               
        ]);


        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

      

        $Expense = new Expense;

        $Expense->type = $request->input('type');
        $Expense->remark = $request->input('remark');
        $Expense->date = $request->input('date');
        $Expense->from_date  = $request->input('from_date');
        $Expense->to_date = $request->input('to_date');
        $Expense->employee_id = $request->input('employee_id');
        $Expense->paid_to = $request->input('paid_to');
        $Expense->paid_by = $request->input('paid_by');
        $Expense->payment_mode = $request->input('payment_mode');
        

        $Expense->save();


        return response()->json(['success'=>'true','message'=>'Expense has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Expense  $Expense
     * @return \Illuminate\Http\Response
     */
    public function show(Expense $expense)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Expense $expense)
    {
        $id = $request->id;
        $employees = Employee::pluck('name','id');
        $expense = Expense::where('id',$id)->first();
        
        return view('expenses.ajax.edit',compact('expense','employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function updateExpense(Request $request)
    {
        
       $validator = Validator::make($request->all(), [
                
                'type' => 'required',
                'date' => 'required',

               
        ]);


        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->first()]);
        }

      

        $Expense = Expense::find($request->id);

        $Expense->type = $request->input('type');
        $Expense->remark = $request->input('remark');
        $Expense->date = $request->input('date');
        $Expense->from_date  = $request->input('from_date');
        $Expense->to_date = $request->input('to_date');
        $Expense->employee_id = $request->input('employee_id');
        $Expense->paid_to = $request->input('paid_to');
        $Expense->paid_by = $request->input('paid_by');
        $Expense->payment_mode = $request->input('payment_mode');
        

        $Expense->save();


        return response()->json(['success'=>'true','message'=>'Expense has been updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expense $expense)
    {
        $expense->delete();
        flash('Expense Deleted Successfully')->success();
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use App\ItemPrice;
use Illuminate\Http\Request;

class ItemPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemPrice  $itemPrice
     * @return \Illuminate\Http\Response
     */
    public function show(ItemPrice $itemPrice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemPrice  $itemPrice
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemPrice $itemPrice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemPrice  $itemPrice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemPrice $itemPrice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemPrice  $itemPrice
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemPrice $itemPrice)
    {
        //
    }
}

<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Dealer extends Model
{	

	use SoftDeletes;
   	public function items()
   	{
   		return $this->hasMany(ItemPrice::class);
   	}

   	public function sales()
   	{
   		return $this->hasMany(SalePrice::class);
   	}
}

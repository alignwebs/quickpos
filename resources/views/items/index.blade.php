@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12 text-right">
            <button class="btn btn-info" id="addItem">Add New</button>
        </div>
    </div>
    <br>
      
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">List of Items</h4>
     
            <div class="table-responsive">
              @include('flash::message')
              <div id="success"></div>
              <div id="result"></div>


            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

 
<!-- Edit Modal -->
<div class="modal fade" id="editItemModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="editResult">
    


    </div>
  </div>
</div>
<!-- add Modal -->
<div class="modal fade" id="itemModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add New Item</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div id="errors"></div>
        {!! Form::open(['route'=>'item.store','class'=>'form-material','id'=>'addItemForm']) !!}

        <div class="form-group">
          <label>Item Name</label>
          {!! Form::text('name',null,['class'=>'form-control form-control-line']) !!}
        </div>
        <div class="form-group">
          <label>Image</label>
          {!! Form::file('image',['class'=>'form-control form-control-line']) !!}
        </div>
        <div class="row">
        @foreach($dealers as $key => $dealer)
        {!! Form::hidden('dealer_id[]',$dealer->id) !!}
        <div class="col-md-4">
          <div class="form-group">
            @if($key ==0)
            <label><b>Dealer</b></label>
            @endif
            <p>{{ $dealer->name }}</p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
             @if($key ==0)
            <label><b>Cost Price</b></label>
             @endif
            {!! Form::text('cost_price[]',null,['class'=>'form-control form-control-line']) !!}
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
             @if($key ==0)
            <label><b>Sell Price</b></label>
             @endif
            {!! Form::text('sell_price[]',null,['class'=>'form-control form-control-line']) !!}
          </div>
        </div>
        @endforeach
        </div>
      <div class="modal-footer">
        <input type="submit" value="Submit" class="btn btn-info">

      </div>

      {!! Form::close() !!}
      <!-- Modal footer -->


    </div>
  </div>
</div>
@endsection




@push('scripts')
 <script src="http://malsup.github.com/jquery.form.js"></script> 
<script>
  var dealerTable;
  $(document).ready(function(){

    getItem();
    $("#addItem").click(function(){
      $("#errors").text("");
      $("#itemModal").modal();
    });






  $('#addItemForm').ajaxForm(function (data) {

    


    


        if(data.errors)
        {
          $("#errors").html('<div class="alert alert-danger">'+data.errors+'</div>');

        }else{

          if(data.success == 'true')
          {
            $("#itemModal").modal('hide');
            getItem();
            $("#success").html('<div class="alert alert-success">'+data.message+'</div>');
            $('#addItemForm')[0].reset();

          }else{

            $("#errors").html('<div class="alert alert-danger">Something went wrong</div>');

          }

        }

    

  });

  });

  function getItem()
  {
    $("#result").html("Loading...");
    $.get('{{ route("item.list") }}',function(data){

     $("#result").html(data);

      $('#itemTable').DataTable();

     deleteConfirm();

   });
  }

  function edit(id)
  {

   $("#editItemModal").modal();

   $.get("{{ route('edit.item') }}", {id:id} ,function(data){


        $("#editResult").html(data);
    
   });

 }

 function deleteConfirm()
 {
  $('.deleteFrm').on('submit', function () {

    var title = $(this).find('.btn-delete').attr('data-title');

    var poll = confirm("Confirm Delete "+title+" ?");

    if(poll)
    {
      return true;
    }
    else
    {
      return false;
    }

  })
}

</script>
@endpush

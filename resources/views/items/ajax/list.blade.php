
<table id="itemTable" class="display nowrap table table-hover table-striped" cellspacing="0" width="100%">
    <thead>
      <tr>
        
          <th>Name</th>
          <th>Image</th>
          <th>Created at</th>
          <th width="220">Manage</th>
      </tr>
  </thead>

    <tbody>

     @foreach($rows as $row)
    <tr>
        
        <td>{{ $row->name }}</td>
        <td>
         <img src="{{ asset('storage/'.$row->image) }}" width="50" class="pull-left">
        </td>
        <td>{{ $row->created_at }}</td>
        <td>
          {!! ModelBtn('item', $row->id) !!}
         </td>
    </tr>
@endforeach

    </tbody>
  </table>

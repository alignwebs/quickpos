    {!! Form::model($item, ['route' => ['update.item', $item->id],'id'=>'editItemForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Item</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors2"></div>

        {!! Form::hidden('id',$item->id) !!}

          <div class="form-group">
            <label>Name</label>
            {!! Form::text('name',null,['class'=>'form-control form-control-line']) !!}
          </div>
           <div class="form-group">
          <label>Image</label>
          {!! Form::file('image',['class'=>'form-control form-control-line']) !!}
          </div>
          <div class="row">
          @foreach($dealers as $key => $dealer)
          {!! Form::hidden('dealer_id[]',$dealer->id) !!}
          <div class="col-md-4">
            <div class="form-group">
              @if($key ==0)
              <label><b>Dealer</b></label>
              @endif
              <p>{{ $dealer->name }}</p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
               @if($key ==0)
              <label><b>Cost Price</b></label>
               @endif
              {!! Form::text('cost_price[]',$itemPrices[$key]->cost_price,['class'=>'form-control form-control-line']) !!}
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
               @if($key ==0)
              <label><b>Sell Price</b></label>
               @endif
              {!! Form::text('sell_price[]',$itemPrices[$key]->sell_price,['class'=>'form-control form-control-line']) !!}
            </div>
          </div>
          @endforeach
          </div>
        
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-info">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">
      $('#editItemForm').ajaxForm(function (data) {

    



            if(data.errors)
            {
              $("#errors2").html('<div class="alert alert-danger">'+data.errors+'</div>');

            }else{

              if(data.success == 'true')
              {
                $("#editItemModal").modal('hide');
                getItem();
                $("#success").html('<div class="alert alert-success">'+data.message+'</div>');
                $('#editItemForm')[0].reset();

              }else{

                $("#errors2").html('<div class="alert alert-danger">Something went wrong</div>');

              }

            }



      });

    </script>

@extends('layouts.app')

@section('content')

  <div class="row">
      <div class="col-12 text-right">
          <button class="btn btn-info" id="addEmployee">Add New</button>
      </div>
  </div>
  <br>
    
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">List of Employees</h4>

            <div class="table-responsive">
              @include('flash::message')
              <div id="success"></div>
              <div id="result"></div>


            </div>
          </div>
        </div>
      </div>
    </div>
  

<!-- Edit Modal -->
<div class="modal fade" id="editEmployeeModal">
  <div class="modal-dialog">
    <div class="modal-content" id="editResult">
    


    </div>
  </div>
</div>
<!-- add Modal -->
<div class="modal fade" id="employeeModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add New Employee</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div id="errors"></div>
        {!! Form::open(['route'=>'employee.store','class'=>'form-material','id'=>'addEmployeeForm']) !!}

        <div class="form-group">
          <label>Name</label>
          {!! Form::text('name',null,['class'=>'form-control form-control-line']) !!}
        </div>
         <div class="form-group">
          <label>Mobile</label>
          {!! Form::text('mobile',null,['class'=>'form-control form-control-line']) !!}
        </div>
         <div class="form-group">
          <label>Address</label>
          {!! Form::text('address',null,['class'=>'form-control form-control-line']) !!}
        </div>
         <div class="form-group">
          <label>Employee Type</label>
          {!! Form::select('emp_type',getEmpTypes(),null,['class'=>'form-control form-control-line']) !!}
        </div>

      
 
      <div class="modal-footer">
        <input type="submit" value="Submit" class="btn btn-info">

      </div>

      {!! Form::close() !!}
      <!-- Modal footer -->


    </div>
  </div>
</div>
@endsection




@push('scripts')

<script>
  var dealerTable;
  $(document).ready(function(){

    getEmployee();
    $("#addEmployee").click(function(){
      $("#errors").text("");
      $("#employeeModal").modal();
    });


  $('#addEmployeeForm').submit(function () {

    


    $.post("{{ route('employee.store') }}", $(this).serialize(), function(data){


        if(data.errors)
        {
          $("#errors").html('<div class="alert alert-danger">'+data.errors+'</div>');

        }else{

          if(data.success == 'true')
          {
            $("#employeeModal").modal('hide');
            getEmployee();
            $("#success").html('<div class="alert alert-success">'+data.message+'</div>');
            $('#addEmployeeForm')[0].reset();

          }else{

            $("#errors").html('<div class="alert alert-danger">Something went wrong</div>');

          }

        }

    });




    return false;

  });

  });

  function getEmployee()
  {
    $("#result").html("Loading...");
    $.get('{{ route("employee.list") }}',function(data){

     $("#result").html(data);

    $('#employeeTable').dataTable();

     deleteConfirm();

   });
  }

  function edit(id)
  {

   $("#editEmployeeModal").modal();

   $.get("{{ route('edit.employee') }}", {id:id} ,function(data){


        $("#editResult").html(data);
    
   });

 }

 function deleteConfirm()
 {
  $('.deleteFrm').on('submit', function () {

    var title = $(this).find('.btn-delete').attr('data-title');

    var poll = confirm("Confirm Delete "+title+" ?");

    if(poll)
    {
      return true;
    }
    else
    {
      return false;
    }

  })
}

</script>
@endpush

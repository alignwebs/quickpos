    {!! Form::model($employee, ['route' => ['update.employee', $employee->id],'id'=>'editEmployeeForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Item</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors2"></div>

        {!! Form::hidden('id',$employee->id,["id"=>"employee_id"]) !!}

          <div class="form-group">
            <label>Name</label>
            {!! Form::text('name',null,['class'=>'form-control form-control-line']) !!}
          </div>
           <div class="form-group">
            <label>Mobile</label>
            {!! Form::text('mobile',null,['class'=>'form-control form-control-line']) !!}
          </div>
           <div class="form-group">
            <label>Address</label>
            {!! Form::text('address',null,['class'=>'form-control form-control-line']) !!}
          </div>
           <div class="form-group">
            <label>Employee Type</label>
            {!! Form::select('emp_type',getEmpTypes(),null,['class'=>'form-control form-control-line']) !!}
          </div>

        
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-info">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editEmployeeForm').submit(function () {

 

        $.post("{{ route('update.employee') }}",$(this).serialize(), function(data){


            if(data.errors)
            {
              $("#errors2").html('<div class="alert alert-danger">'+data.errors+'</div>');

            }else{

              if(data.success == 'true')
              {
                $("#editEmployeeModal").modal('hide');
                getEmployee();
                $("#success").html('<div class="alert alert-success">'+data.message+'</div>');
                $('#editEmployeeForm')[0].reset();

              }else{

                $("#errors2").html('<div class="alert alert-danger">Something went wrong</div>');

              }

            }

        });




        return false;

      });

    </script>

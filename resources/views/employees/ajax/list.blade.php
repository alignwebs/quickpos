
<table id="employeeTable" class="display nowrap table table-hover table-striped" cellspacing="0" width="100%">
    <thead>
      <tr>
        
          <th>Name</th>
          <th>Mobile</th>
          <th>Address</th>
          <th>Employee Type</th>
          <th>Created at</th>
          <th width="220">Manage</th>
      </tr>
  </thead>

    <tbody>

     @foreach($rows as $row)
    <tr>
        
        <td>{{ $row->name }}</td>
        <td>{{ $row->mobile }}</td>
        <td>{{ $row->address }}</td>
        <td>{{ $row->emp_type }}</td>
        
        <td>{{ $row->created_at }}</td>
        <td>
          {!! ModelBtn('employee', $row->id) !!}
         </td>
    </tr>
@endforeach

    </tbody>
  </table>

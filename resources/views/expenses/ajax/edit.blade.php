    {!! Form::model($expense, ['route' => ['update.expense', $expense->id],'id'=>'editExpenseForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Expense</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors2"></div>

        {!! Form::hidden('id',$expense->id) !!}

          
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Type</label>
                {!! Form::select('type',['salary'=>'Salary','misc'=>'Misc'],null,['class'=>'form-control form-control-line']) !!}
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Remark</label>
                {!! Form::text('remark',null,['class'=>'form-control form-control-line']) !!}
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>From Date</label>
                {!! Form::text('from_date',null,['class'=>'form-control form-control-line datepicker']) !!}
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>To Date</label>
                {!! Form::text('to_date',null,['class'=>'form-control form-control-line datepicker']) !!}
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Date</label>
                {!! Form::text('date',null,['class'=>'form-control form-control-line datepicker']) !!}
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Employee</label>
                {!! Form::select('employee_id',$employees,null,['class'=>'form-control form-control-line']) !!}
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Paid To</label>
                {!! Form::text('paid_to',null,['class'=>'form-control form-control-line']) !!}
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Paid By</label>
                {!! Form::select('paid_by',$employees,null,['class'=>'form-control form-control-line','placeholder'=>'Select Paid By']) !!}
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Payment Mode</label>
                {!! Form::text('payment_mode',null,['class'=>'form-control form-control-line']) !!}
              </div>
            </div>
          </div>

        
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-info">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editExpenseForm').submit(function () {

   

        $.post("{{ route('update.expense') }}",$(this).serialize(), function(data){


            if(data.errors)
            {
              $("#errors2").html('<div class="alert alert-danger">'+data.errors+'</div>');

            }else{

              if(data.success == 'true')
              {
                $("#editExpenseModal").modal('hide');
                getExpense();
                $("#success").html('<div class="alert alert-success">'+data.message+'</div>');
                $('#editExpenseForm')[0].reset();

              }else{

                $("#errors2").html('<div class="alert alert-danger">Something went wrong</div>');

              }

            }

        });




        return false;

      });

    </script>

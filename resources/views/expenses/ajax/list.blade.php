
<table id="expenseTable" class="display nowrap table table-hover table-striped" cellspacing="0" width="100%">
    <thead>
      <tr>
        
          <th>Type</th>
          <th>Remark</th>
          <th>Date</th>
          <th>From Date</th>
          <th>To Date</th>
          <th>Employee</th>
          <th>Paid To</th>
          <th>Paid By</th>
          <th>Payment Mode</th>
          <!-- <th>Created at</th> -->
          <th width="220">Manage</th>
      </tr>
  </thead>

    <tbody>

     @foreach($rows as $row)
    <tr>
        
        <td>{{ $row->type }}</td>
        <td>{{ $row->remark }}</td>
        <td>{{ $row->date }}</td>
        <td>{{ $row->from_date }}</td>
        <td>{{ $row->to_date }}</td>
        <td>{{ $row->employee->name }}</td>
        <td>{{ $row->paid_to }}</td>
        <td>{{ $row->employee->name }}</td>
        <td>{{ $row->payment_mode }}</td>
        
        <!-- <td>{{ $row->created_at }}</td> -->
        <td>
          {!! ModelBtn('expense', $row->id) !!}
         </td>
    </tr>
@endforeach

    </tbody>
  </table>

@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-12 text-right">
            <button class="btn btn-info" id="addExpense">Add New</button>
        </div>
    </div>
    <br>
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">List of Expenses</h4>
           
            <div class="table-responsive">
              @include('flash::message')
              <div id="success"></div>
              <div id="result"></div>


            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Edit Modal -->
<div class="modal fade" id="editExpenseModal">
  <div class="modal-dialog">
    <div class="modal-content" id="editResult">
    


    </div>
  </div>
</div>
<!-- add Modal -->
<div class="modal fade" id="expenseModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add New Expense</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div id="errors"></div>
        {!! Form::open(['route'=>'expense.store','class'=>'form-material','id'=>'addExpenseForm']) !!}

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Type</label>
              {!! Form::select('type',['salary'=>'Salary','misc'=>'Misc'],null,['class'=>'form-control form-control-line']) !!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Remark</label>
              {!! Form::text('remark',null,['class'=>'form-control form-control-line']) !!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>From Date</label>
              {!! Form::text('from_date',null,['class'=>'form-control form-control-line FromDatepicker']) !!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>To Date</label>
              {!! Form::text('to_date',null,['class'=>'form-control form-control-line ToDatepicker','disabled']) !!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Date</label>
              {!! Form::text('date',null,['class'=>'form-control form-control-line datepicker']) !!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Employee</label>
              {!! Form::select('employee_id',$employees,null,['class'=>'form-control form-control-line','placeholder'=>'Select Employee']) !!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Paid By</label>
              {!! Form::select('paid_by',$employees,null,['class'=>'form-control form-control-line','placeholder'=>'Select Paid By']) !!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Paid To</label>
              {!! Form::text('paid_to',null,['class'=>'form-control form-control-line']) !!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Payment Mode</label>
              {!! Form::text('payment_mode',null,['class'=>'form-control form-control-line']) !!}
            </div>
          </div>
        </div>
      
 
      <div class="modal-footer">
        <input type="submit" value="Submit" class="btn btn-info">

      </div>

      {!! Form::close() !!}
      <!-- Modal footer -->


    </div>
  </div>
</div>
</div>
@endsection




@push('scripts')

<script>
  var expenseTable;

        
  
  $(document).ready(function(){

    getExpense();
    $("#addExpense").click(function(){
      $("#errors").text("");
      $("#expenseModal").modal();
    });


     $('.datepicker').flatpickr();

     $(".FromDatepicker").flatpickr({

        onChange: function(selectedDates, dateStr, instance) {
              
            $(".ToDatepicker").prop('disabled',false);  

            $(".ToDatepicker").flatpickr({

              minDate:dateStr

            })
        }

     });

  $('#addExpenseForm').submit(function () {

   


    $.post("{{ route('expense.store') }}",$(this).serialize(), function(data){


        if(data.errors)
        {
          $("#errors").html('<div class="alert alert-danger">'+data.errors+'</div>');

        }else{

          if(data.success == 'true')
          {
            $("#expenseModal").modal('hide');
            getExpense();
            $("#success").html('<div class="alert alert-success">'+data.message+'</div>');
            $('#addExpenseForm')[0].reset();

          }else{

            $("#errors").html('<div class="alert alert-danger">Something went wrong</div>');

          }

        }

    });




    return false;

  });

  });

  function getExpense()
  {
    $("#result").html("Loading...");

    $.get('{{ route("expense.list") }}',function(data){

     $("#result").html(data);

      $('#expenseTable').DataTable();
     
     deleteConfirm();

     });
  }

  function edit(id)
  {

   $("#editExpenseModal").modal();

   $.get("{{ route('edit.expense') }}", {id:id} ,function(data){


        $("#editResult").html(data);
        $('.datepicker').flatpickr();
   });

 }

 function deleteConfirm()
 {
  $('.deleteFrm').on('submit', function () {

    var title = $(this).find('.btn-delete').attr('data-title');

    var poll = confirm("Confirm Delete "+title+" ?");

    if(poll)
    {
      return true;
    }
    else
    {
      return false;
    }

  })
}

</script>
@endpush

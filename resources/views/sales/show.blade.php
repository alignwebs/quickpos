@extends('layouts.app')

@section('content')

      <div class="row">
      <div class="col-6">
        <div class="card">
          <div class="card-body">

            <h5>Sale #{{ $sale->id }} </h5>
            <table class="table table-bordered table-condensed">
              <tr>
                <th>Name</th>
                <td>{{ $sale->name }}</td>
              </tr>

              <tr>
                <th>Delivery Employee</th>
                <td>{{ $sale->employee->name }}</td>
              </tr>
              <tr>
                <th>Delivery Fee</th>
                <td>{{ $sale->delivery_fee }}</td>
              </tr>
              <tr>
                <th>Amount Payable</th>
                <td>{{ $sale->amount_payable }}</td>
              </tr>
               <tr>
                <th>Amount Paid</th>
                <td>
                  {!! Form::open(['route'=>'sale.amount.update','class'=>'form-material','id'=>'amountPaidForm']) !!}
                  {!! Form::hidden('id',$sale->id,['id'=>'id'])  !!}
                  {!! Form::text('amount_paid',$sale->amount_paid,['id'=>'amount_paid'])  !!} 
                   <input type="submit" value="Update">
                  {!! Form::close() !!}
                </td>
              </tr>
              <tr>
                <th>Status</th>
                <td>
                {!! Form::select('status',status(),$sale->status,['id'=>'status'])  !!} 
                </td>
              </tr>
               <tr>
                <th>Comments</th>
                <td>{{ $sale->comments }}</td>
              </tr>

            </table>

          </div>
        </div>
      </div>
            <div class="col-6">
        <div class="card">
          <div class="card-body">

            <h5>Customer</h5>
            <table class="table table-bordered table-condensed">
              <tr>
                <th>Name</th>
                <td>{{ $sale->customer->name }}</td>
              </tr>
              <tr>
                <th>Mobile</th>
                <td>{{ $sale->customer->mobile }}</td>
              </tr>
              <tr>
                <th>Address</th>
                <td>{{ $sale->customer->address }}</td>
              </tr>

            </table>

          </div>
        </div>
      </div>

    </div>
    <br>
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">

            <h5>Sale Items</h5>
            <table class="table table-bordered table-condensed">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Dealer Name</th>
                  <th>Cost Price</th>
                  <th>Sell Price</th>
                  <th>Quantity</th>
                  <th>Dealer Paid</th>

                </tr>
              </thead>
              <tbody>
                @foreach($sale->sale_item as $item)
                  <tr>
                    <td>{{ $item->item_name }}</td>
                    <td>{{ $item->dealer_name }}</td>
                    <td>{{ $item->cost_price }}</td>
                    <td>{{ $item->sell_price }}</td>
                    <td>{{ $item->quantity }}</td>
                    <td>{{ $item->dealer_paid }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>

@endsection




@push('scripts')
<script type="text/javascript">
    
$("#amountPaidForm").submit(function(){

  $.post("{{ route('sale.amount.update') }}",$(this).serialize(), function(data){


      if(data.errors)
      {
          alert(data.errors);

      }else{

        if(data.success == 'true')
        {

          alert(data.message);


        }else{

          alert('Something went wrong.');

        }

      }

  });


  return false;

});

$("#status").change(function(){
    
    var id = $("#id").val();
    var status = $(this).val();

    $.post("{{ route('sale.status.update') }}",{id:id,status:status,"_token": "{{ csrf_token() }}"}, function(data){

        if(data.errors)
        {
            alert(data.errors);

        }else{

          if(data.success == 'true')
          {

            alert(data.message);


          }else{

            alert('Something went wrong.');

          }

        }

    });

});

</script>

@endpush


<table id="saleTable" class="display nowrap table table-hover table-striped" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th>Sale #</th> 
          <th>Customer</th>
          <th>Delivery Employee</th>
          <th>Date</th>
          <th width="220">Manage</th>
      </tr>
  </thead>
    <tbody>
     @foreach($rows as $row)
    <tr>
        <td>{{ $row->id }}</td>
        <td>{{ $row->customer->name }}</td>
        <td>{{ $row->employee->name }}</td>
        <td>{{ $row->date }}</td>
        <td>
          <div class="btn-group btn-options" role="group">
          {!! Form::open(['route' => ['sale.destroy', $row->id], 'method' => 'delete', 'class' => 'deleteFrm']) !!}
          <a href="{{ route('sale.show',$row->id) }}" class="btn btn-default btn-sm"><span class="fa fa-eye" aria-hidden="true"></span></a>
          <button type="submit" class="btn-delete btn btn-default btn-sm delete-btn"  data-title="{{ $row->id }}"><span class="fa fa-trash" aria-hidden="true"></span></button>
        {!! Form::close() !!}
          </div>
         </td>
    </tr>
    @endforeach

    </tbody>
  </table>

@extends('layouts.app')

@section('content')


      <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">List of Sales</h4>
            
            <div class="table-responsive">
              @include('flash::message')
              <div id="success"></div>
              <div id="result"></div>


            </div>
          </div>
        </div>
      </div>
    </div>

@endsection




@push('scripts')

<script>
  var dealerTable;
  $(document).ready(function(){

    getSale();


  });

  function getSale()
  {
    $("#result").html("Loading...");
    $.get('{{ route("sale.list") }}',function(data){

     $("#result").html(data);

    $('#saleTable').dataTable();

     deleteConfirm();

   });
  }


 function deleteConfirm()
 {
  $('.deleteFrm').on('submit', function () {

    var title = $(this).find('.btn-delete').attr('data-title');

    var poll = confirm("Confirm Delete "+title+" ?");

    if(poll)
    {
      return true;
    }
    else
    {
      return false;
    }

  })
}

</script>
@endpush

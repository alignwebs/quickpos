@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                              <div class="card-body">Total Customers <br><span class="total">{{ $customers}}</span></div>
                              
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                              <div class="card-body">Total Sales <br><span class="total">{{ $sales }}</span></div>
                              
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                              <div class="card-body">Total Employees <br><span class="total">{{ $employees}}</span></div>
                              
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                              <div class="card-body">Total Items <br><span class="total">{{ $items }}</span></div>
                              
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


<table id="dealerTable" class="display nowrap table table-hover table-striped" cellspacing="0" width="100%">
    <thead>
      <tr>
        
          <th>Name</th>
          <th>Latitude</th>
          <th>Longitude</th>
          
          <th>Address</th>
          <th>City</th>
          <th>State</th>
          <th>Country</th>
          <th>Created at</th>
          <th width="220">Manage</th>
      </tr>
  </thead>

    <tbody>

     @foreach($rows as $row)
    <tr>
        
        <td>{{ $row->name }}</td>
        <td>{{ $row->loc_lat }}</td>
        <td>{{ $row->loc_lon }}</td>
        
        <td>{{ $row->address }}</td>
        <td>{{ $row->city }}</td>
        <td>{{ $row->state }}</td>
        <td>{{ $row->country }}</td>
        
        <td>{{ $row->created_at }}</td>
        <td>
          {!! ModelBtn('dealer', $row->id) !!}
         </td>
    </tr>
@endforeach

    </tbody>
  </table>

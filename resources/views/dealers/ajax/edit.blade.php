    {!! Form::model($dealer, ['route' => ['update.dealer', $dealer->id],'id'=>'editDealerForm','class'=>'form-material']) !!}

    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Edit Dealer</h4>
      <button type="button" class="close" data-dismiss="modal">×</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" >


      <div id="errors2"></div>

        {!! Form::hidden('id',$dealer->id) !!}

        {!! Form::hidden('loc_lat',null,['id'=>'loc_lat2'])  !!}
          {!! Form::hidden('loc_lon',null,['id'=>'loc_lon2'])  !!}
          <div class="form-group">
            <label>Name</label>
            {!! Form::text('name',null,['class'=>'form-control form-control-line']) !!}
          </div>
          
          <div class="form-group">
            <label>Name</label>
            {!! Form::text('name',null,['class'=>'form-control form-control-line']) !!}
          </div>
          <div class="form-group">
            <label>Delivery Range Km</label>
            {!! Form::number('delivery_range_km',null,['class'=>'form-control form-control-line']) !!}
          </div>
          <div class="form-group">
            <label>Address</label>
            {!! Form::text('address',null,['class'=>'form-control form-control-line','id'=>'address2']) !!}
          </div>
          <div class="form-group">
            <label>City</label>
            {!! Form::text('city',null,['class'=>'form-control form-control-line','id'=>'city',"readonly"]) !!}
          </div>
          <div class="form-group">
            <label>State</label>
            {!! Form::text('state',null,['class'=>'form-control form-control-line','id'=>'state',"readonly"]) !!}
          </div>
          <div class="form-group">
            <label>Country</label>
            {!! Form::text('country',null,['class'=>'form-control form-control-line','id'=>'country',"readonly"]) !!}
          </div>
          
        
        
    </div>

        <div class="modal-footer">
          <input type="submit" value="Submit" class="btn btn-info">

        </div>


    <!-- Modal footer -->

    {!! Form::close() !!}

    <script type="text/javascript">

      $('#editDealerForm').submit(function () {

   

        $.post("{{ route('update.dealer') }}",$(this).serialize(), function(data){


            if(data.errors)
            {
              $("#errors2").html('<div class="alert alert-danger">'+data.errors+'</div>');

            }else{

              if(data.success == 'true')
              {
                $("#editDealerModal").modal('hide');
                getDealer();
                $("#success").html('<div class="alert alert-success">'+data.message+'</div>');
                $('#editDealerForm')[0].reset();

              }else{

                $("#errors2").html('<div class="alert alert-danger">Something went wrong</div>');

              }

            }

        });




        return false;

      });



    </script>

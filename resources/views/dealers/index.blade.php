@extends('layouts.app')

@section('content')
<style type="text/css">
  .pac-container {
    background-color: #FFF;
    z-index: 20;
    position: fixed;
    display: inline-block;
    float: left;
}
.modal{
    z-index: 20;   
}
.modal-backdrop{
    z-index: 10;        
}​
</style>
    <div class="row">
        <div class="col-12 text-right">
            <button class="btn btn-info" id="addDealer">Add New</button>
        </div>
    </div>
    <br>
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">List of Dealers</h4>
           
            <div class="table-responsive">
              @include('flash::message')
              <div id="success"></div>
              <div id="result"></div>


            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Edit Modal -->
<div class="modal fade" id="editDealerModal">
  <div class="modal-dialog">
    <div class="modal-content" id="editResult">
    


    </div>
  </div>
</div>
<!-- add Modal -->
<div class="modal fade" id="dealerModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add New Dealer</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div id="errors"></div>
        {!! Form::open(['route'=>'dealer.store','class'=>'form-material','id'=>'addDealerForm']) !!}
        {!! Form::hidden('loc_lat',null,['id'=>'loc_lat'])  !!}
        {!! Form::hidden('loc_lon',null,['id'=>'loc_lon'])  !!}
        
        <div class="form-group">
          <label>Name</label>
          {!! Form::text('name',null,['class'=>'form-control form-control-line']) !!}
        </div>
        <div class="form-group">
          <label>Delivery Range Km</label>
          {!! Form::number('delivery_range_km',null,['class'=>'form-control form-control-line']) !!}
        </div>
        <div class="form-group">
          <label>Address</label>
          {!! Form::text('address',null,['class'=>'form-control form-control-line','id'=>'address']) !!}
        </div>
        <div id="addressDetails">

        </div>
      
 
      <div class="modal-footer">
        <input type="submit" value="Submit" class="btn btn-info">

      </div>

      {!! Form::close() !!}
      <!-- Modal footer -->


    </div>
  </div>
</div>
@endsection




@push('scripts')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOdlQTErcFDIacFIEhjBQjUjCseLKJheI&libraries=places"></script>

<script type="text/javascript">

  var IsplaceChange = false;
      $(document).ready(function () {            
          var input = document.getElementById('address');
          var options = {
           componentRestrictions: {country: "in"}
          };
          var autocomplete = new google.maps.places.Autocomplete(input,options);

          google.maps.event.addListener(autocomplete, 'place_changed', function () {
              var result = autocomplete.getPlace();
              var lat = result.geometry.location.lat();
              var lng = result.geometry.location.lng();
              $("#loc_lat").val(lat);
              $("#loc_lon").val(lat);
              for(var i = 0; i < result.address_components.length; i += 1) {
                var addressObj = result.address_components[i];
                for(var j = 0; j < addressObj.types.length; j += 1) {

                  if (addressObj.types[j] === 'administrative_area_level_2') {
                    
                    var city = addressObj.long_name;
                  }

                  if (addressObj.types[j] === 'administrative_area_level_1') {
                    
                    var state = addressObj.long_name;
                  }
                  
                  if (addressObj.types[j] === 'country') {
                    
                    var country = addressObj.long_name;
                  }                  
                }
              }

              $("#addressDetails").html('<div class="form-group"><label>City</label><input type="text" id="city" name="city" value="'+city+'" class="form-control" readonly></div><div class="form-group"><label>State</label><input type="text" id="state" name="state" value="'+state+'" class="form-control" readonly></div><div class="form-group"><label>Country</label><input type="text" id="country" name="country" value="'+country+'" class="form-control" readonly></div>');
          });


});

</script>
<script>
  var dealerTable;
  $(document).ready(function(){

    getDealer();
    $("#addDealer").click(function(){
      $("#errors").text("");
      $("#dealerModal").modal();
    });






  $('#addDealerForm').submit(function () {

   


    $.post("{{ route('dealer.store') }}",$(this).serialize(), function(data){


        if(data.errors)
        {
          $("#errors").html('<div class="alert alert-danger">'+data.errors+'</div>');

        }else{

          if(data.success == 'true')
          {
            $("#dealerModal").modal('hide');
            getDealer();
            $("#success").html('<div class="alert alert-success">'+data.message+'</div>');
            $('#addDealerForm')[0].reset();

          }else{

            $("#errors").html('<div class="alert alert-danger">Something went wrong</div>');

          }

        }

    });




    return false;

  });

  });

  function getDealer()
  {
    $("#result").html("Loading...");

    $.get('{{ route("dealer.list") }}',function(data){

     $("#result").html(data);

      $('#dealerTable').DataTable();

     deleteConfirm();

     });
  }

  function edit(id)
  {

   $("#editDealerModal").modal();

   $.get("{{ route('edit.dealer') }}", {id:id} ,function(data){


        $("#editResult").html(data);

        var input = document.getElementById('address2');
        var options = {
         componentRestrictions: {country: "in"}
        };
        var autocomplete = new google.maps.places.Autocomplete(input,options);

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var result = autocomplete.getPlace();
            var lat = result.geometry.location.lat();
            var lng = result.geometry.location.lng();
            $("#loc_lat").val(lat);
            $("#loc_lon").val(lat);
            for(var i = 0; i < result.address_components.length; i += 1) {
              var addressObj = result.address_components[i];
              for(var j = 0; j < addressObj.types.length; j += 1) {

                if (addressObj.types[j] === 'administrative_area_level_2') {
                  
                  var city = addressObj.long_name;
                }

                if (addressObj.types[j] === 'administrative_area_level_1') {
                  
                  var state = addressObj.long_name;
                }
                
                if (addressObj.types[j] === 'country') {
                  
                  var country = addressObj.long_name;
                }                  
              }
            }

            $("#city").val(city);
            $("#state").val(state);
            $("#country").val(country);
        });


    
   });

 }

 function deleteConfirm()
 {
  $('.deleteFrm').on('submit', function () {

    var title = $(this).find('.btn-delete').attr('data-title');

    var poll = confirm("Confirm Delete "+title+" ?");

    if(poll)
    {
      return true;
    }
    else
    {
      return false;
    }

  })
}

</script>
@endpush

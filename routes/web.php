<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>'auth'], function () {
		// DEALER
		Route::get('/edit/dealer', 'DealerController@edit')->name('edit.dealer');
		Route::post('/update/dealer', 'DealerController@updateDealer')->name('update.dealer');
		Route::get('/dealer/list', 'DealerController@list')->name('dealer.list');
		Route::resource('dealer','DealerController');
		// END DEALER

		// Employee
		Route::get('/edit/employee', 'EmployeeController@edit')->name('edit.employee');
		Route::post('/update/employee', 'EmployeeController@updateEmployee')->name('update.employee');
		Route::get('/employee/list', 'EmployeeController@list')->name('employee.list');
		Route::resource('employee','EmployeeController');
		// END Employee

		// EXPENSE
		Route::get('/edit/expense', 'ExpenseController@edit')->name('edit.expense');
		Route::post('/update/expense', 'ExpenseController@updateExpense')->name('update.expense');
		Route::get('/expense/list', 'ExpenseController@list')->name('expense.list');
		Route::resource('expense','ExpenseController');
		// END EXPENSE


		// ITEM
		Route::get('/edit/item', 'ItemController@edit')->name('edit.item');
		Route::post('/update/item', 'ItemController@updateItem')->name('update.item');
		Route::get('/item/list', 'ItemController@list')->name('item.list');
		Route::resource('item','ItemController');
		// END ITEM


		// SALE
		Route::get('/edit/sale', 'SaleController@edit')->name('edit.sale');
		Route::post('/update/sale', 'SaleController@updateSale')->name('update.sale');
		Route::get('/sale/list', 'SaleController@list')->name('sale.list');
		Route::post('/sale/amount/update', 'SaleController@amountPaidUpdate')->name('sale.amount.update');
		Route::post('/sale/status/update', 'SaleController@statusUpdate')->name('sale.status.update');
		Route::resource('sale','SaleController');
		// END SALE
});


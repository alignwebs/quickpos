<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/item/list', 'ItemController@itemList');
Route::get('/employee/list', 'EmployeeController@listAjax');
Route::get('/customer/find/mobile', 'CustomerController@findByMobile');

Route::post('sale/add', 'SaleController@add');